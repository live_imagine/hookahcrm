package ru.kpfu.itis.hookahCRM.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.kpfu.itis.hookahCRM.entities.Hello;

@Repository
public interface HelloRepository extends JpaRepository<Hello, Long>{

    @Query(value = "select abc from Hello where abc = 131")
    public int getOne();

}
