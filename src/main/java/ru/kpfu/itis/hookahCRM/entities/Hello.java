package ru.kpfu.itis.hookahCRM.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "test_table")
public class Hello {

    private long abc;

    @Id
    @Column(name = "abc")
    public long getAbc() {
        return abc;
    }

    public void setAbc(long abc) {
        this.abc = abc;
    }

}
