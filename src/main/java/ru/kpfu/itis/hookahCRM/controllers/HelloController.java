package ru.kpfu.itis.hookahCRM.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.kpfu.itis.hookahCRM.entities.Hello;
import ru.kpfu.itis.hookahCRM.repositories.HelloRepository;

@Controller
public class HelloController {
    private HelloRepository helloRepository;

    @Autowired
    public void setHelloRepository(HelloRepository helloRepository) {
        this.helloRepository = helloRepository;
    }

    @RequestMapping("/hello")
    public String hello(ModelMap mp) {
        mp.put("number", helloRepository.getOne());
        return "hello";
    }

}
