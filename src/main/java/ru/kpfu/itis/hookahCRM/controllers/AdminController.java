package ru.kpfu.itis.hookahCRM.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.kpfu.itis.hookahCRM.repositories.UserRepository;

@Controller
public class AdminController {
    @Autowired
    private UserRepository userRepository;

    @RequestMapping("/admin")
    public String index() {
        return "admin/index";
    }


    @RequestMapping("admin/users")
    public String users(Model model) {
        model.addAttribute("users", userRepository.findAll());
        return "admin/users";
    }


}
