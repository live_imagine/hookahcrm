package ru.kpfu.itis.hookahCRM.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ManageController {
    @RequestMapping("/manage")
    public String index() {
        return "manage/index";
    }
}
